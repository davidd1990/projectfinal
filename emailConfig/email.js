const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
let transporter = nodemailer.createTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: 'postmaster@blumuntechnology.com',
        pass: 'e66c3acf4ba9f3cf9b4822473d2a04f7'
    }
})
transporter.use('compile', hbs({
    viewPath: 'template',
    extName: '.hbs'
}))
module.exports.transporter = transporter