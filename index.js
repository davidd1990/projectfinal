const {logger} = require('./log/config')
const morgan = require("morgan")
const express = require("express")
const cors = require('cors')
const app = express()
const db = require('./starup/dbORM')
const db2 = require('./starup/dbORM')
const RateLimit = require('express-rate-limit')



db
    .authenticate()
    .then(() => {
    console.log(`Connection has been established successfully. en modo  ${process.env.NODE_ENV}`);
})
.catch(err => {
    console.error('Unable to connect to the database:', err);
})

db2
    .authenticate()
    .then(() => {
        console.log(`Connection2 has been established successfully. en modo  ${process.env.NODE_ENV}`);
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    })



var limiter = new RateLimit({
    windowMs: 15*60*1000, // 15 minutes
    max: 50, // limit each IP to 100 requests per windowMs
    delayMs: 0 // disable delaying - full speed until the max limit is reached
});

//  apply to all requests
app.use(cors())

require('./starup/routes')(app)
require('./starup/config')()
//
process.on('uncaughtException', (ex) => {
    logger.error(ex.message,ex)
process.exit(1)
})


process.on('unhandledRejection', (ex) => {
    logger.error(ex.message,ex)
process.exit(1)
})



//




if (app.get("env") === "development") {
    app.use(morgan("tiny"))
}


const port = process.env.PORT || 3000
const serve = app.listen(port, () => { logger.info(`Listing on port ${port}....`)})
module.exports = serve
