const jwt = require('jsonwebtoken')
const config = require('config')

module.exports = function (req, res, next) {
    // validate token in headers
    const token = req.header('x-auth-token')
    if (!token) return res.status(401).send('Access Denied no token provide')
    try {
        const decode = jwt.verify(token,config.get('jwtPrivateKey'))
        req.user = decode
        next()
    }
    catch (ex) {
        res.status(400).send('Token no es valido')
    }
    //validate token
}
