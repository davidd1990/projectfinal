const {logger} = require('../log/config')
module.exports = function (err, req, res,next) {
    logger.error(err.message, err)
    res.status(500).send('There is a problema contact IT Service')
    next()
}