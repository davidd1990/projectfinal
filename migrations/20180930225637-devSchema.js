'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
      queryInterface.createTable(
          'users',
          {
              idUsers: {
                  allowNull: false,
                  autoIncrement: true,
                  type: Sequelize.INTEGER,
                  primaryKey: true
              },
//
              nombres: {
                  type: Sequelize.STRING
              },
              apellidos: {
                  type: Sequelize.STRING
              },
              identificacion: {
                  type: Sequelize.STRING
              },
              correo: {
                  type: Sequelize.STRING
              },
              fuerzaPublica: {
                  type: Sequelize.STRING
              },
              rango: {
                  type: Sequelize.STRING
              },
              idFuerza: {
                  type: Sequelize.INTEGER,
              },
              contrasenia: {
                  type: Sequelize.STRING
              },
              estado: {
                  type: Sequelize.BOOLEAN,
                  defaultValue: 0
              }
          }
      )
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.dropTable('usuarios')
  }
};
