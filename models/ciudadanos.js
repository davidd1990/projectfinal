const Joi = require('joi')
const config = require('config')
const db = require('../starup/dbmysqRegis')
const _ = require('lodash')
const Sequelize = require('sequelize')

const Op = Sequelize.Op

const Ciudadano = db.define('ciudadanos',{
    idciudadanos: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    nombres: {
        type: Sequelize.STRING
    },
    apellidos: {
        type: Sequelize.STRING
    },
    fechaNacimiento: {
        type: Sequelize.DATE
    },
    fechaExpedicion: {
        type: Sequelize.DATE
    },
    lugarNacimiento: {
        type: Sequelize.INTEGER
    },
    lugarExpedicion: {
        type: Sequelize.INTEGER
    },
    rh: {
        type: Sequelize.STRING
    },
    gruposanguineo: {
        type: Sequelize.STRING
    },
    identificacion: {
        type: Sequelize.STRING,
    },
    estatura: {
        type: Sequelize.STRING
    },
    requerido: {
        type: Sequelize.BOOLEAN,
    },
    estado: {
        type: Sequelize.STRING,
        defaultValue: 1
    },
})

Ciudadano.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return Ciudadano.findAndCountAll({
        where: {
            requerido: state,
            [Op.or]: [
                { nombres: {
                    [Op.like]: '%' + search + '%'
                }},
                { apellidos: {
                    [Op.like]: '%' + search + '%'
                }},
                { identificacion: {
                    [Op.like]: '%' + search + '%'
                }}
            ]

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return Ciudadano.findAll({
                where: {
                    requerido: state,
                    [Op.or]: [
                        { nombres: {
                            [Op.like]: '%' + search + '%'
                        }},
                        { apellidos: {
                            [Op.like]: '%' + search + '%'
                        }},
                        { identificacion: {
                            [Op.like]: '%' + search + '%'
                        }}
                    ]
                },
                limit: limit,
                offset: offset,
                $sort: { idciudadano: 1 },
                include: [{all:true}]
            })
                .then((c) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = c
                    return pagination

                })
        })
        .catch(function (error) {
            return error
        })
}
Ciudadano.indexCiudadano = function (req) {

    let limit = parseInt(req.query.npp || 10000)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return Ciudadano.findAndCountAll({
        where: {
            requerido: state,
            [Op.or]: [
                { nombres: {
                    [Op.like]: '%' + search + '%'
                }},
                { apellidos: {
                    [Op.like]: '%' + search + '%'
                }}
            ]

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return Ciudadano.findAll({
                where: {
                    requerido: state,
                    [Op.or]: [
                        { nombres: {
                            [Op.like]: '%' + search + '%'
                        }},
                        { apellidos: {
                            [Op.like]: '%' + search + '%'
                        }}
                    ]
                },
                limit: limit,
                offset: offset,
                $sort: { idciudadano: 1 },
                include: [{all:true}]
            })
                .then((c) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = c
                    return pagination

                })
        })
        .catch(function (error) {
            return error
        })
}
Ciudadano.updateMy = async function (req) {


    // validate if there is already a Ciudadano with the same email
    let result = {}
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return result
    }
    let c = await Ciudadano.findById(id)
    if (!c)  return 'Ciudadano not found'

    req.body.identificacionUpdate = c.identificacion

    let validate = await Ciudadano.validationId(req)
    if (!validate) {
        result = {
            status: false,
            msg : 'Ciudadano con esta identificacion ya esta registrado'

        }
        return result
    }
    result = await c.update({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        fechaNacimiento: req.body.fechaNacimiento,
        lugarNacimiento: req.body.lugarNacimiento,
        fechaExpedicion: req.body.fechaExpedicion,
        lugarExpedicion: req.body.lugarExpedicion,
        rh: req.body.rh,
        gruposanguineo: req.body.gruposanguineo,
        identificacion: req.body.identificacion,
        estatura: req.body.estatura,
        requerido: req.body.requerido


    }).then(async () => {
        result = {
            status: true,
            msg : 'Ciudadano actualizado satisfactoriamente'

        }
        return result

    })
    return result
}

Ciudadano.insert = function(req) {

    const result = Ciudadano.create({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        fechaNacimiento: req.body.fechaNacimiento,
        lugarNacimiento: req.body.lugarNacimiento,
        fechaExpedicion: req.body.fechaExpedicion,
        lugarExpedicion: req.body.lugarExpedicion,
        rh: req.body.rh,
        gruposanguineo: req.body.gruposanguineo,
        identificacion: req.body.identificacion,
        estatura: req.body.estatura,
        requerido: req.body.requerido
    }).then((Ciudadano)=> {
        return Ciudadano
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id
Ciudadano.getCiudadano = function (req) {
    return Ciudadano.findOne({
        where: {idciudadanos: req},
        include: [{all:true}]
    }).then(Ciudadano => {
        return Ciudadano
    })

}

Ciudadano.getCiudadanoById = function (req) {

    return Ciudadano.findOne({
        where: {identificacion: req},
        include: [{all:true}]
    }).then(c => {
        return c
    })

}

Ciudadano.getCiudadanoConfirmation = function (req) {
    return Ciudadano.findOne({
        where: {identificacion: req}
    }).then(Ciudadano => {
        return Ciudadano
    })

}


Ciudadano.deleteObj = async function (ciudadano) {
   let  result =  ciudadano.destroy().then(() =>{

        return {
            status: true,
            msg: 'Ciudadano Eliminado correctamente'
        }
    })
    return result
}

Ciudadano.validationId = function (req) {

    if (req.body.identificacionUpdate === req.body.identificacion) {
        return true
    } else {
        return Ciudadano.findOne({
            where: {
                identificacion: req.body.identificacion
            }
        }).then((user) => {
            if (isEmpty(user)){
                return true
            } else{
                return false
            }
        })
    }

}

Ciudadano.validateCiudadano = function  (ciudadano) {
    const schema = {
        nombres: Joi.string().min(5).max(50).required(),
        apellidos: Joi.string().min(5).max(50).required(),
        fechaNacimiento: Joi.date().min('1-1-1950'),
        lugarNacimiento: Joi.number().integer().required(),
        lugarExpedicion: Joi.number().integer().required(),
        rh: Joi.string().min(1).max(3).required(),
        gruposanguineo: Joi.string().min(1).max(3).required(),
        identificacion: Joi.string().min(5).max(255).required(),
        estatura: Joi.string().min(1).max(20).required(),
        fechaExpedicion: Joi.date().min('1-1-1950'),
        requerido: Joi.boolean()
    }
    return Joi.validate(ciudadano, schema)
}


module.exports = Ciudadano