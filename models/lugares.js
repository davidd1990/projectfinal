const db = require('../starup/dbORM')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const lugares = db.define('municipios',{
    municipio: {
        type: Sequelize.STRING
    },
    estado: {
        type: Sequelize.STRING
    },
    id_municipio: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    departamento_id: {
        type: Sequelize.INTEGER
    }

})
lugares.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let offset = 0
    return lugares.findAndCountAll({
        where: {
            NombreLugar: {
                [Op.like]: '%' + search + '%'
            }

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return lugares.findAll({
                where: {
                    NombreLugar: {
                        [Op.like]: '%' + search + '%'
                    }

                },
                limit: limit,
                offset: offset,
                $sort: { idLugar: 1 },
                include: [{all:true}]
            })
                .then((results) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = results
                    return pagination

                })
        })
        .catch(function () {
            return false
        })
}


module.exports= lugares