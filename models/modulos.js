const Joi = require('joi')
const db = require('../starup/dbUser')
const _ = require('lodash')
const Sequelize = require('sequelize')

const Op = Sequelize.Op

const Modulos = db.define('modulos',{

    idmodulos: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    moduloname: {
        type: Sequelize.STRING
    },
    icono: {
        type: Sequelize.STRING
    },
    url: {
        type: Sequelize.STRING
    }
})

Modulos.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let offset = 0
    return Modulos.findAndCountAll({
        where: {
            Modulos: {
                [Op.like]: '%' + search + '%'
            }
        }
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return Modulos.findAll({
                where: {
                    Modulos: {
                        [Op.like]: '%' + search + '%'
                    }
                },
                limit: limit,
                offset: offset,
            })
                .then((t) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = t
                    return pagination
                })
        })
        .catch(function (err) {
            return err
        })
}

Modulos.updateMy = async function (req) {


    // validate if there is already a Modulos with the same email
    let result = {}
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return result
    }
    let t= await Modulos.findById(id)
    if (!t)  return 'Modulos not found'
    req.body.emailUpdate = t.correo
    let validate = await Modulos.validationModulos(req)
    if (!validate){
        result = {
            status: false,
            msg : 'Tipo Usuario con este nombre ya esta registrado'

        }
        return result
    }
    result = await t.update({
        Modulos: req.body.Modulos
    }).then(async () => {
        result = {
            status: true,
            msg : 'Tipo Usuario actualizado satisfactoriamente'

        }
        return result

    })
    return result
}

Modulos.insert = function(req) {

    const result = Modulos.create({
        Modulos: req.body.Modulos
    }).then((t)=> {
        return t
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id

Modulos.getModulos = function (req) {
    return Modulos.findOne({
        where: {idModuloss: req}
    }).then( t => {
        return t
    })

}


Modulos.deleteObj = async function (req) {

    result =  req.destroy().then(() =>{

        return {
            status: true,
            msg: 'Tipo Usuario Eliminado correctamente'
        }
    })
    return result
}


Modulos.validationModulos = function(req) {

    if (req.body.emailModulos === req.body.Modulos) return  true
    return Modulos.findOne({
        where: {Modulos: req.body.Modulos}
    }).then(t => {
        if (isEmpty(t)) return true
        return false
    })
}


Modulos.validateModulos = function  (Modulos) {
    const schema = {
        Modulos: Joi.string().min(5).max(255).required()
    }
    return Joi.validate(Modulos, schema)
}


module.exports = Modulos