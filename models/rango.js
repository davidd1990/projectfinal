const Joi = require('joi')
const db = require('../starup/dbORM')
const _ = require('lodash')
const Sequelize = require('sequelize')

const Op = Sequelize.Op

const Rango = db.define('rango',{
    idRango: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    rango: {
        type: Sequelize.STRING
    },
    estado: {
        type: Sequelize.BOOLEAN,
        defaultValue: 1
    }
})

Rango.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return Rango.findAndCountAll({
        where: {
            estado: state,
            rango: {
                [Op.like]: '%' + search + '%'
            }

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return Rango.findAll({
                where: {
                    estado: state,
                    rango: {
                        [Op.like]: '%' + search + '%'
                    }
                },
                limit: limit,
                offset: offset,
                $sort: { idUser: 1 },
                include: [{all:true}]
            })
                .then((rango) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = rango
                    return pagination

                })
        })
        .catch(function () {
            return false
        })
}

Rango.updateMy = async function (req) {


    // validate if there is already a user with the same email
    let result = {}
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return result
    }
    let objet = await Rango.findById(id)
    if (!objet)  return 'Rango no encontrado'
    console.log(objet.rango)
    req.body.rangoUpdate = objet.rango

    let validate = await Rango.validateNombreRango(req)
    if (!validate){
        result = {
            status: false,
            msg : 'Rango con este nombre ya esta registrado'

        }
        return result
    }

    result = await objet.update({
        rango: req.body.rango,
    }).then(async () => {
        result = {
            status: true,
            msg : 'Rango actualizado satisfactoriamente'

        }
        return result

    })
    return result
}

Rango.insert = function(req) {

    const result = Rango.create({
        rango: req.body.rango
    }).then((result)=> {
        return result
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id

Rango.validateNombreRango = function(req) {

    if (req.body.rangoUpdate === req.body.rango) return  true
    return Rango.findOne({
        where: {rango: req.body.rango}
    }).then(result => {
        if (isEmpty(result)) return true
        return false
    })
}

Rango.getRango = function (req) {
    return Rango.findOne({
        where: {idRango: req}
    }).then(user => {
        return user
    })

}

Rango.deleteObj = async function (req) {

    result =  req.destroy().then(() =>{

        return {
            status: true,
            msg: 'Rango Eliminado correctamente'
        }
    })
    return result
}

Rango.validateRango = function  (user) {
    const schema = {
        rango: Joi.string().min(5).max(50).required()
    }
    return Joi.validate(user, schema)
}


Rango.validateRangoUpdate = function (user) {
    const schema = {
        rango: Joi.string().min(5).max(50).required(),
        idRango: Joi.number().integer().required()
    }
    return Joi.validate(user, schema)
}



module.exports = Rango