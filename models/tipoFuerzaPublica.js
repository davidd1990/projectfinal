const Joi = require('joi')
const db = require('../starup/dbORM')
const _ = require('lodash')
const Sequelize = require('sequelize')

const Op = Sequelize.Op

const TipoFuerza = db.define('fuerzasPublicas',{
    idFuerzaPublica: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    nombre: {
        type: Sequelize.STRING
    },
    estado: {
        type: Sequelize.BOOLEAN,
        defaultValue: 1
    }
})

TipoFuerza.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return TipoFuerza.findAndCountAll({
        where: {
            estado: state,
            nombre: {
                [Op.like]: '%' + search + '%'
            }

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return TipoFuerza.findAll({
                where: {
                    estado: state,
                    nombre: {
                        [Op.like]: '%' + search + '%'
                    }
                },
                limit: limit,
                offset: offset
            })
                .then((TipoFuerza) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = TipoFuerza
                    return pagination

                })
        })
        .catch(function () {
            return false
        })
}

TipoFuerza.updateMy = async function (req) {


    // validate if there is already a user with the same email
    let result = {}
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return result
    }
    let objet = await TipoFuerza.findById(id)
    if (!objet)  return 'Fuerza Publica  no encontrada'

    req.body.TipoFuerzaUpdate = objet.nombre

    let validate = await TipoFuerza.validateNombreTipoFuerza(req)
    if (!validate){
        result = {
            status: false,
            msg : 'Fuerza Publica con este nombre ya esta registrado'

        }
        return result
    }

    result = await objet.update({
        nombre: req.body.nombre,
    }).then(async () => {
        result = {
            status: true,
            msg : 'Fueza Publica actualizada satisfactoriamente'

        }
        return result

    })
    return result
}

TipoFuerza.validateNombreTipoFuerza = function(req) {

    if (req.body.tipoFuerzaUpdate === req.body.nombre) return  true
    return TipoFuerza.findOne({
        where: {nombre: req.body.nombre}
    }).then(result => {
        if (isEmpty(result)) return true
        return false
    })
}

TipoFuerza.insert = function(req) {

    const result = TipoFuerza.create({
        nombre: req.body.nombre
    }).then((result)=> {
        return result
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id

TipoFuerza.getTipoFuerzaPublica = function (req) {

    return TipoFuerza.findOne({
        where: {idFuerzaPublica: req}
    }).then(user => {
        return user
    })

}
// pRUEBA
TipoFuerza.deleteObj = async function (req) {

    result =  req.destroy().then(() =>{

        return {
            status: true,
            msg: 'Fuerza Publica Eliminada correctamente'
        }
    })
    return result
}


TipoFuerza.validateTipoFuerza = function  (user) {
    const schema = {
        nombre: Joi.string().min(5).max(50).required()
    }
    return Joi.validate(user, schema)
}

TipoFuerza.validateTipoFuerzaUpdate = function (user) {
    const schema = {
        nombre: Joi.string().min(5).max(50).required(),
        idFuerza: Joi.number().integer().required()
    }
    return Joi.validate(user, schema)
}

module.exports = TipoFuerza