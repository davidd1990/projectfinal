const Joi = require('joi')
const db = require('../starup/dbUser')
const _ = require('lodash')
const Sequelize = require('sequelize')

const Op = Sequelize.Op

const TipoUsuario = db.define('tipoUsuarios',{

    idTipoUsuarios: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    tipoUsuario: {
        type: Sequelize.STRING
    }
})

TipoUsuario.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let offset = 0
    return TipoUsuario.findAndCountAll({
        where: {
            tipoUsuario: {
                [Op.like]: '%' + search + '%'
            }
        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return TipoUsuario.findAll({
                where: {
                    tipoUsuario: {
                        [Op.like]: '%' + search + '%'
                    }
                },
                limit: limit,
                offset: offset,
                include: [{all:true}]
            })
                .then((t) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = t
                    return pagination
                })
        })
        .catch(function (err) {
            return err
        })
}

TipoUsuario.updateMy = async function (req) {


    // validate if there is already a TipoUsuario with the same email
    let result = {}
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return result
    }
    let t= await TipoUsuario.findById(id)
    if (!t)  return 'TipoUsuario not found'
    req.body.emailUpdate = t.correo
    let validate = await TipoUsuario.validationTipoUsuario(req)
    if (!validate){
        result = {
            status: false,
            msg : 'Tipo Usuario con este nombre ya esta registrado'

        }
        return result
    }
    result = await t.update({
        tipoUsuario: req.body.tipoUsuario
    }).then(async () => {
        result = {
            status: true,
            msg : 'Tipo Usuario actualizado satisfactoriamente'

        }
        return result

    })
    return result
}

TipoUsuario.insert = function(req) {

    const result = TipoUsuario.create({
        tipoUsuario: req.body.tipoUsuario
    }).then((t)=> {
        return t
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id

TipoUsuario.getTipoUsuario = function (req) {
    return TipoUsuario.findOne({
        where: {idTipoUsuarios: req}
    }).then( t => {
        return t
    })

}


TipoUsuario.deleteObj = async function (req) {

    result =  req.destroy().then(() =>{

        return {
            status: true,
            msg: 'Tipo Usuario Eliminado correctamente'
        }
    })
    return result
}


TipoUsuario.validationTipoUsuario = function(req) {

    if (req.body.emailTipoUsuario === req.body.tipoUsuario) return  true
    return TipoUsuario.findOne({
        where: {tipoUsuario: req.body.tipoUsuario}
    }).then(t => {
        if (isEmpty(t)) return true
        return false
    })
}


TipoUsuario.validateTipoUsuario = function  (TipoUsuario) {
    const schema = {
        tipoUsuario: Joi.string().min(5).max(255).required()
    }
    return Joi.validate(TipoUsuario, schema)
}


module.exports = TipoUsuario