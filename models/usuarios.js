const Joi = require('joi')
const jwt = require('jsonwebtoken')
const config = require('config')
const db = require('../starup/dbUser')
const _ = require('lodash')
const Sequelize = require('sequelize')
const tipoUsuario = require('./tipoUser')
const Op = Sequelize.Op

const Usuarios = db.define('usuarios',{
    idUser: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    nombres: {
        type: Sequelize.STRING
    },
    apellidos: {
        type: Sequelize.STRING
    },
    identificacion: {
        type: Sequelize.STRING
    },
    correo: {
        type: Sequelize.STRING
    },
    contrasena: {
        type: Sequelize.STRING
    },
    idTipoUsuarios: {
        type: Sequelize.INTEGER
    },
    estado: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
    }
})

Usuarios.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return Usuarios.findAndCountAll({
        where: {
            estado: state,
            [Op.or]: [
                { nombres: {
                    [Op.like]: '%' + search + '%'
                }},
                { apellidos: {
                    [Op.like]: '%' + search + '%'
                }}
            ]

        },
        include: [
            {
                model:tipoUsuario,
                include: [{all:true}] }
            ]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return Usuarios.findAll({
                where: {
                    estado: state,
                    [Op.or]: [
                        { nombres: {
                            [Op.like]: '%' + search + '%'
                        }},
                        { apellidos: {
                            [Op.like]: '%' + search + '%'
                        }}
                        ]

                },
                attributes: ['idUser','correo','idTipoUsuarios','estado','nombres','apellidos','identificacion'],
                limit: limit,
                offset: offset,
                $sort: { idUsuarios: 1 },
                include: [
                    {
                        model:tipoUsuario,
                        include: [{all:true}] }
                ]
            })
                .then((u) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = u
                    return pagination
                })
        })
        .catch(function (err) {
            return err
        })
}

Usuarios.updateMy = async function (req,u ) {


    // validate if there is already a Usuarios with the same email

   let  result = await u.update({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        identificacion: req.body.identificacion,
        idTipoUsuarios: req.body.idTipoUsuarios,
        correo: req.body.correo

    }).then(async (u) => {


        return u

    })
    return result
}

Usuarios.insert = function(req) {

    const result = Usuarios.create({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        correo: req.body.correo,
        contrasena: req.body.contrasena,
        idTipoUsuarios: req.body.idTipoUsuarios,
        identificacion: req.body.identificacion,
    }).then((u)=> {
        return u
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id
Usuarios.getUser = function (req) {
    return Usuarios.findOne({
        where: {idUser: req},
        attributes: ['idUser','correo','idTipoUsuarios','estado','nombres','apellidos','identificacion'],
        include: [
            {
                model:tipoUsuario,
                include: [{all:true}] }
        ]
    }).then(u => {
        return u
    })

}

Usuarios.getUserById = function (req) {
    return Usuarios.findOne({
        where: {identificacion: req}
    }).then(u => {
        return u
    })

}
Usuarios.getUserByIdUser = function (req) {
    return Usuarios.findOne({
        where: {idUser: req}
    }).then(u => {
        return u
    })

}

Usuarios.getUsuariosConfirmation = function (req) {
    return Usuarios.findOne({
        where: {idUsuarios: req}
    }).then(u => {
        return u
    })

}

Usuarios.authenticateUser = function (req) {
    return Usuarios.findOne({
        where: {
            correo: req.body.correo,
            constrasenia: req.body.contrasenia
        }
    }).then(Usuarios => {
        return Usuarios
    })
}

Usuarios.deleteObj = async function (req) {

    const result =  req.update({
        estado: '0'
    }).then(() =>{

        return {
            status: true,
            msg: 'Usuario Eliminado correctamente'
        }
    })
    return result
}

Usuarios.actived = async function (req) {

    let id = req.query.id || ''
    if (id === '') return false
    let u = await Usuarios.findById(id)
    const result =  u.update({
        EstadoPersona: 'Activo'
    }).then(() =>{
        return true
    })
    return result
}

Usuarios.validationEmail = function(req) {

    if (req.body.emailUpdate === req.body.correo) return  true
    return Usuarios.findOne({
        where: {correo: req.body.correo}
    }).then(u => {
        if (isEmpty(u)) return true
        return false
    })
}

Usuarios.generateAuthToken = function (user) {

    const token = jwt.sign({Usuarios: user.idUser,tipoUsuario: user.tipoUsuario.tipoUsuario, exp: Math.floor(Date.now() / 1000) + (60 * 60)}, config.get('jwtPrivateKey'))
    return token
}

Usuarios.generateAuthTokenConfirmation = function (id) {

    const token = jwt.sign({_id: id}, config.get('jwtPrivateKeyEmail'))
    return token
}

Usuarios.generateAuthTokenPasswordReset = function (Usuarios) {

    const token = jwt.sign({_id: Usuarios.identificacion}, config.get('jwtPrivateKeyPasswordReset'))
    return token
}

Usuarios.validateConfirmationToken  = function (token) {
    try {
        const decode = jwt.verify(token,config.get('jwtPrivateKeyEmail'))
        return decode
    } catch (ex) {

        return ''
    }
}

Usuarios.validateResetPasswordToken=   function  (token) {
    try {
        const decode = jwt.verify(token,config.get('jwtPrivateKeyPasswordReset'))
        return decode
    } catch (ex) {

        return ''
    }
}

Usuarios.validateUserPasswordReset  = function (Usuarios) {
    const schema = {
        password: Joi.string().min(5).max(255).required(),
        password_confirmation: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } }),
        token:  Joi.string()
    }
    return Joi.validate(Usuarios, schema)
}

Usuarios.validateUser = function  (Usuarios) {
    const schema = {
        correo: Joi.string().min(5).max(255).required().email(),
        contrasena: Joi.string().min(5).max(255).required(),
        nombres: Joi.string().min(5).max(50).required(),
        apellidos: Joi.string().min(5).max(50).required(),
        identificacion: Joi.string().min(5).max(50).required(),
        idFuerza: Joi.number().integer().required(),
        idRango: Joi.number().integer().required(),
        idTipoUsuarios: Joi.number().integer().required(),
        identificacionFuerza: Joi.string().min(5).max(50).required()

    }
    return Joi.validate(Usuarios, schema)
}

Usuarios.validateUserAuth = function  (Usuarios) {
    const schema = {
        correo: Joi.string().min(5).max(255).required().email(),
        contrasena: Joi.string().min(5).max(255).required()
    }
    return Joi.validate(Usuarios, schema)
}

Usuarios.validateUserUpdate = function (Usuarios) {
    const schema = {
        correo: Joi.string().min(5).max(255).required().email(),
        nombres: Joi.string().min(5).max(50).required(),
        apellidos: Joi.string().min(5).max(50).required(),
        identificacion: Joi.string().min(5).max(50).required(),
        idUser: Joi.number().integer().required(),
        idFuerza: Joi.number().integer().required(),
        idRango: Joi.number().integer().required(),
        idTipoUsuarios: Joi.number().integer().required(),
        identificacionFuerza: Joi.string().min(1).max(50)
    }
    return Joi.validate(Usuarios, schema)
}

Usuarios.validationId = function (req) {

    if (req.body.identificacionUpdate === req.body.identificacion) {
        return true
    } else {
        return Usuarios.findOne({
            where: {
                identificacion: req.body.identificacion
            }
        }).then((u) => {
            if (isEmpty(u)){
                return true
            } else{
                return false
            }
        })
    }

}

Usuarios.validateUserEmail= function  (Usuarios) {
    const schema = {
        correo: Joi.string().min(5).max(255).required().email()
    }
    return Joi.validate(Usuarios, schema)
}

Usuarios.getUserEmail= function  (correo) {
    return Usuarios.findOne({
        where: {correo: correo}
    }).then( u  => {
        return u
    })
}

module.exports = Usuarios
