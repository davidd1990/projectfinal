const Joi = require('joi')
const db = require('../starup/dbORM')
const db2 = require('../starup/dbORM')
const _ = require('lodash')
const Sequelize = require('sequelize')
const User = require('./usuarios')
const Op = Sequelize.Op

const fuerzaPublica = db.define('usuarios',{
    idusuarios: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
        primaryKey: true
    },
//
    identificacion: {
        type: Sequelize.STRING
    },
    idRango: {
        type: Sequelize.INTEGER,
    },
    idFuerza: {
        type: Sequelize.INTEGER,
    },
    identificacionFuerza: {
        type: Sequelize.STRING
    },
    idUser: {
        type: Sequelize.INTEGER
    }
})

fuerzaPublica.index = function (req) {

    let limit = parseInt(req.query.npp || 10)   // number of records per page
    let search = req.query.search || ''
    search = decodeURI(search)
    let state = req.query.state || '1'
    let offset = 0
    return fuerzaPublica.findAndCountAll({
        where: {
            estado: state

        },
        include: [{all:true}]
    })
        .then((data) => {
            let page = parseInt(req.query.page) || 1      // page number
            let pages = Math.ceil(data.count / limit)
            offset = limit * (page - 1)
            return fuerzaPublica.findAll({
                where: {
                    estado: state,
                    nombres: {
                        [Op.like]: '%' + search + '%'
                    }
                },
                limit: limit,
                offset: offset,
                $sort: { idUserFuerzaPublica: 1 },
                include: [{all:true}]
            })
                .then((fuerzapublica) => {
                    let pagination = {
                        totalItem: data.count,
                        current: page,
                        perPage: limit,
                        pages: pages,
                        result: ''

                    }
                    pagination.result = fuerzapublica
                    return pagination

                })
        })
        .catch(function () {
            return false
        })
}

fuerzaPublica.updateMy = async function (req, id) {



    let f = await fuerzaPublica.getUserByIdUser(id)

   let result = await f.update({
        identificacion: req.body.identificacion,
        fuerzaPublica: req.body.fuerzaPublica,
        idFuerza: req.body.idFuerza,
        idRango: req.body.idRango


    }).then(async () => {
       let r = {
           status: true,
           msg : 'personal Fuerza Publica actualizado satisfactoriamente'

       }
       return r

    })
    return result
}

fuerzaPublica.insert = function(req) {

    const result = fuerzaPublica.create({
        identificacion: req.body.identificacion,
        idRango: req.body.idRango,
        idFuerza: req.body.idFuerza,
        idUser: req.body.idUser,
        identificacionFuerza: req.body.identificacionFuerza,
    }).then((user)=> {
        return user
    })
    return result

}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
//obtenemos un usuario por su id
fuerzaPublica.getFuerzaPublica = function (req) {
    return fuerzaPublica.findOne({
        where: {idUserFuerzaPublica: req}
    }).then(user => {
        return user
    })

}

fuerzaPublica.getUserById = function (req) {
    return fuerzaPublica.findOne({
        where: {idUserFuerzaPublica: req}
    }).then(user => {
        return user
    })

}

fuerzaPublica.getUserByIdUser = function (req) {
    console.log(req)
    return fuerzaPublica.findOne({
        where: {idUser: req},
        include: [{all:true}]
    }).then(user => {
        console.log(user)
        return user
    })

}
fuerzaPublica.getFuerzaByIdFuerza = function (req) {

    return fuerzaPublica.findOne({
        where: {
            identificacionFuerza: req.identificacionFuerza
        },
        include: [{all:true}]
    }).then(user => {
        console.log(user)
        return user
    })

}




fuerzaPublica.deleteObj = async function (req) {

    result =  req.update({
        estado: '0'
    }).then(() =>{

        return {
            status: true,
            msg: 'personal Fuerza Publica Eliminado correctamente'
        }
    })
    return result
}




fuerzaPublica.validateFuerzaPublica = function  (user) {
    const schema = {
        nombres: Joi.string().min(5).max(50).required(),
        apellidos: Joi.string().min(5).max(50).required(),
        identificacion: Joi.string().min(5).max(50).required(),
        idFuerza: Joi.number().integer().required(),
        idRango: Joi.number().integer().required(),
        tipoUsuario: Joi.number().integer().required(),
        correo: Joi.string().min(5).max(255).required().email(),
        identificacionFuerza: Joi.string().min(5).max(255).required()
    }
    return Joi.validate(user, schema)
}



fuerzaPublica.validateUserUpdate = function (user) {
    const schema = {
        nombres: Joi.string().min(5).max(50).required(),
        apellidos: Joi.string().min(5).max(50).required(),
        identificacion: Joi.string().min(5).max(50).required(),
        idFuerza: Joi.number().integer().required(),
        rango: Joi.string().min(5).max(255).required(),
        identificacionFuerza: Joi.string().min(5).max(255).required().email()
    }
    return Joi.validate(user, schema)
}

fuerzaPublica.validationId = function (req) {

    if (req.body.identificacionUpdate === req.body.identificacion) {
        return true
    } else {
        return fuerzaPublica.findOne({
            where: {
                identificacion: req.body.identificacion
            }
        }).then((user) => {
            if (isEmpty(user)){
                return true
            } else{
                return false
            }
        })
    }

}
fuerzaPublica.validationEmail = function (req) {

    if (req.body.emailUpdate === req.body.correo) {
        return true
    } else {
        return fuerzaPublica.findOne({
            where: {
                correo: req.body.correo
            }
        }).then((user) => {
            if (isEmpty(user)){
                return true
            } else{
                return false
            }
        })
    }

}


module.exports = fuerzaPublica