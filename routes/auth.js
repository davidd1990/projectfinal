const express = require('express')
const Joi = require('joi')
const bcrypt = require('bcrypt')
const _ = require('lodash')
const router = express.Router()
const User = require('../models/usuarios')
const tipoUsuario = require('../models/tipoUser')
var RateLimit = require('express-rate-limit')
const md5 = require('md5')

var createAccountLimiter = new RateLimit({
    windowMs: 60*60*1000, // 1 hour window
    delayAfter: 3, // begin slowing down responses after the first request
    delayMs: 3*1000, // slow down subsequent responses by 3 seconds per request
    max: 50, // start blocking after 5 requests
    message: "Too many accounts created from this IP, please try again after an hour"
});

//
router.post('/', createAccountLimiter , async (req, res) => {
    // validate request
    const {error} =  User.validateUserAuth(req.body)
    if (error) return res.status(400).send(error.details[0].message)
    // validate email of the user
    let user = await User.findOne({
        where: {correo: req.body.correo},
        include: [
            {
                model:tipoUsuario,
                include: [{all:true}] }
        ]
    })
    if (!user) return res.status(400).send('Invalid email or password')
    // validate password
    const validPassword = await bcrypt.compare(req.body.contrasena, user.contrasena)
    if (!validPassword) return res.status(400).send('Invalid email or password')

    if (user.estado === 0 ) return  res.status(400).send({error: 'User is not confirmed comunicate with IT or see you email for confimation token' })
    /*
    // validate if is a actived user
    if (user.isActive === false ) return res.status(400).send({error: 'User is not active'})
    */
    //generate Token
    user = _.pick(user,['idUser','correo','tipoUsuario'])
    const token = await User.generateAuthToken(user)
    res.send({token: token, user: user})
})

function validate (user) {
    const schema = {
        email: Joi.string().min(2).max(255).required().email(),
        password: Joi.string().min(2).max(255).required()
    }
    return Joi.validate(user, schema)
}
module.exports = router