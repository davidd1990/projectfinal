const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const _ = require('lodash')
const Ciudadano = require('../models/ciudadanos')
const router = express.Router()


// prueba
router.get("/",[authMiddleware], async function(req,res){
    const result = await Ciudadano.index(req)
    res.status(200).json(result)
})
router.get("/todasciudadanos",[authMiddleware], async function(req,res){
    const result = await Ciudadano.indexCiudadano(req)
    res.status(200).json(result)
})
router.post('/',[authMiddleware],asyncM( async (req, res) => {

    const {error} =  Ciudadano.validateCiudadano(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    // validate if there is already a Ciudadano with the identificacion
    let  c = await Ciudadano.validationId(req)
    if (!c) return  res.status(400).json({status: 'Ciudadano ya esta registrado'})
    // created Ciudadano in database
    let result = await Ciudadano.insert(req)
    // send email with a token to confirmated the Ciudadano
    const result2 = _.pick(result,['nombres','apellidos','identificacion','createdAt'])
    return res.status(200).send( result2)
    // filter the results avoiding  the password to be seen

//
}))

router.post("/validationEmail", [authMiddleware],  async function(req,res){
    const result = await Ciudadano.validationEmail(req)
    res.status(200).json({result: result})
})

router.post("/validationCiudadano",[authMiddleware], async function(req,res){
    const result = await Ciudadano.validationId(req)
    res.status(200).json({result: result})
})
//
router.get('/:id',[authMiddleware], asyncM( async (req, res) => {
    // q paso
    let c = await Ciudadano.getCiudadano(req.params.id)
    if (!c) res.status(200).json({error: 'ciudadano no registrado'})
    res.status(200).json(c)
}))

router.get('/identificacion/:id',[authMiddleware], asyncM( async (req, res) => {
    // q paso
    let c = await Ciudadano.getCiudadanoById(req.params.id)
    if (!c) res.status(200).json({error: 'ciudadano no registrado'})
    res.status(200).json(c)
}))

router.delete('/',[authMiddleware], asyncM( async (req, res) => {

    let c = await Ciudadano.findById(req.query.id)
    if (!c) return res.status(404).send({error: 'The Ciudadano with the given ID was not found.'})
    let result = await Ciudadano.deleteObj(c)
    return res.status(200).send(result)
}))

router.put('/',[authMiddleware], asyncM( async (req, res) => {

    const {error} =  Ciudadano.validateCiudadano(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    let c = await Ciudadano.updateMy(req)
    //aksdbhkas
    return res.send(c)

}))





module.exports = router