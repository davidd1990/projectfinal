const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const _ = require('lodash')
const FuerzaPublica = require('../models/tipoFuerzaPublica')
const router = express.Router()


// prueba
router.get("/", async function(req,res){
    const result = await FuerzaPublica.index(req)
    res.status(200).json(result)
})

router.post('/',asyncM( async (req, res) => {

    const {error} =  FuerzaPublica.validateTipoFuerza(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    // validate if there is already a FuerzaPublica with the identificacion
    let  c = await FuerzaPublica.validateNombreTipoFuerza(req)
    if (!c) return  res.status(400).json({status: 'FuerzaPublica ya esta registrado'})
    // created FuerzaPublica in database
    let result = await FuerzaPublica.insert(req)
    // send email with a token to confirmated the FuerzaPublica
    return res.status(200).send( result)
    // filter the results avoiding  the password to be seen

//
}))

router.post("/validationEmail", [authMiddleware],  async function(req,res){
    const result = await FuerzaPublica.validationEmail(req)
    res.status(200).json({result: result})
})

router.post("/validationFuerzaPublica",[authMiddleware], async function(req,res){
    const result = await FuerzaPublica.validationIdNumber(req)
    res.status(200).json({result: result})
})
//
router.get('/:id', asyncM( async (req, res) => {
    // q paso
    let c = await FuerzaPublica.getTipoFuerzaPublica(req.params.id)
    if (!c) res.status(200).json({error: 'FuerzaPublica no registrada'})
    res.status(200).json(c)
}))

router.get('/identificacion/:id',[authMiddleware], asyncM( async (req, res) => {
    // q paso
    let c = await FuerzaPublica.getFuerzaPublicaById(req.params.id)
    if (!c) res.status(200).json({error: 'FuerzaPublica no registrado'})
    res.status(200).json(c)
}))

router.delete('/', asyncM( async (req, res) => {

    let c = await FuerzaPublica.findById(req.query.id)
    if (!c) return res.status(404).send({error: 'The FuerzaPublica with the given ID was not found.'})
    let result = await FuerzaPublica.deleteObj(c)
    return res.status(200).send(result)
}))

router.put('/', asyncM( async (req, res) => {

    const {error} =  FuerzaPublica.validateTipoFuerza(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    let c = await FuerzaPublica.updateMy(req)
    //aksdbhkas
    return res.send(c)

}))





module.exports = router