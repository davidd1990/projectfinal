const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const _ = require('lodash')
const Rango = require('../models/rango')
const router = express.Router()


// prueba
router.get("/", async function(req,res){
    const result = await Rango.index(req)
    res.status(200).json(result)
})

router.post('/',[authMiddleware], asyncM( async (req, res) => {

    const {error} =  Rango.validateRango(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    // validate if there is already a Rango with the identificacion
    let  c = await Rango.validateNombreRango(req)
    console.log(c)
    if (!c) return  res.status(400).json({status: 'Rango ya esta registrado'})
    // created Rango in database
    let result = await Rango.insert(req)
    // send email with a token to confirmated the Rango
    return res.status(200).send( result)
    // filter the results avoiding  the password to be seen

//
}))

//
router.get('/:id',[authMiddleware],  asyncM( async (req, res) => {

    let c = await Rango.getRango(req.params.id)
    if (!c) res.status(200).json({error: 'Rango no registrado'})
    res.status(200).json(c)

}))

router.delete('/',[authMiddleware],  asyncM( async (req, res) => {

    let c = await Rango.findById(req.query.id)
    if (!c) return res.status(404).send({error: 'The Rango with the given ID was not found.'})
    let result = await Rango.deleteObj(c)
    return res.status(200).send(result)
}))

router.put('/',[authMiddleware],  asyncM( async (req, res) => {

    const {error} =  Rango.validateRango(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    let c = await Rango.updateMy(req)
    //aksdbhkas
    return res.send(c)

}))





module.exports = router