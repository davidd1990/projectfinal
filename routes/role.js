const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const adminMiddleware = require('../middleware/admin')
const validateObjectId = require('../middleware/validateObjectId')
const bcrypt = require('bcrypt')
const _ = require('lodash')
const {Rol, validateRol} = require('../models/rol.js')
const router = express.Router()
const {transporter} =  require('../emailConfig/email')
const rolModel = require('../models/rol')


router.get("/", async function(req,res){
    const result = await rolModel.getRoles(req)
    res.status(200).json({result: result})
})

router.post("/validationRol",  async function(req,res){
    const result = await rolModel.validationRolName(req)
    res.status(200).json({result: result})
})

//
router.post('/', asyncM( async (req, res) => {
    // default password
    req.body.password = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    const {error} =  validateUser(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user = await User.findOne({email: req.body.email })
    if (user) return res.status(400).send('User already registered.')
    user = new User(_.pick(req.body, ['name', 'email','password','lastname','isAdmin']))
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password,salt)
    //b
    const token = user.generateAuthTokenConfirmation()
    let mailOptions = {
        from: '"Grupo MAT" <confimation@grupomat.com>', // sender address
        to: req.body.email , // list of receivers
        subject: 'Confirmacion de usuario ✔', // Subject line
        template: 'email',
        context: {
            token: token
        }

    };

    await transporter.sendMail(mailOptions, (err) => {
        if (err) {
            return res.status(500).send('Algo salio mal comunicate con el departamento de IT');
        }
    })
    user = await user.save()

    res.send(_.pick(user, ['name', 'email', 'id','lastname']))
}))

router.get('/:id',[validateObjectId, authMiddleware],  asyncM( async (req, res) => {
    const user = await User.findById(req.params.id);
    if (!user) return res.status(404).send({error: 'The user with the given ID was not found.'})
    res.send(_.pick(user, ['name', 'email', 'id','actions','role']))
}))

router.delete('/:id',[validateObjectId, authMiddleware],  asyncM( async (req, res) => {
    let user = await User.findById(req.params.id)
    if (!user) return res.status(404).send({error: 'The user with the given ID was not found.'})
    user.isActive = false
    await user.save()

    res.send({msg: 'Ok'})
}))
router.put('/:id',[validateObjectId, authMiddleware],  asyncM( async (req, res) => {
    const {error} =  validateUser(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user = await User.findById(req.params.id)
    if (!user) return res.status(404).send({error: 'The user with the given ID was not found.'})
    user.name = req.body.name
    user.lastname = req.body.lastname
    user.email = req.body.email
    user.isAdmin = req.body.isAdmin
    user.actions = req.body.actions
    user.role = req.body.role
    await user.save()
    res.send({msg: 'Ok'})

}))



module.exports = router