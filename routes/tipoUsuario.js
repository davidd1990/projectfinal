const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const _ = require('lodash')
const TipoUser = require('../models/tipoUser')
const router = express.Router()


// prueba
router.get("/",[authMiddleware], async function(req,res){
    const result = await TipoUser.index(req)
    res.status(200).json(result)
})

router.post('/',[authMiddleware], asyncM( async (req, res) => {

    const {error} =  TipoUser.validateTipoUsuario(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    // validate if there is already a TipoUser with the identificacion
    let  c = await TipoUser.validationTipoUsuario(req)

    if (!c) return  res.status(400).json({status: 'TipoUser ya esta registrado'})
    // created TipoUser in database
    let result = await TipoUser.insert(req)
    // send email with a token to confirmated the TipoUser
    return res.status(200).send( result)
    // filter the results avoiding  the password to be seen

//
}))

//
router.get('/:id', [authMiddleware], asyncM( async (req, res) => {

    let c = await TipoUser.getTipoUsuario(req.params.id)
    if (!c) res.status(200).json({error: 'TipoUser no registrado'})
    res.status(200).json(c)

}))

router.delete('/', [authMiddleware], asyncM( async (req, res) => {

    let c = await TipoUser.findById(req.query.id)
    if (!c) return res.status(404).send({error: 'The TipoUser with the given ID was not found.'})
    let result = await TipoUser.deleteObj(c)
    return res.status(200).send(result)
}))

router.put('/', [authMiddleware], asyncM( async (req, res) => {

    const {error} =  TipoUser.validateTipoUsuario(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    let c = await TipoUser.updateMy(req)
    //aksdbhkas
    return res.send(c)

}))





module.exports = router