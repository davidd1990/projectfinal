const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const adminMiddleware = require('../middleware/admin')
const validateObjectId = require('../middleware/validateObjectId')
const bcrypt = require('bcrypt')
const _ = require('lodash')
const User = require('../models/usuarios')
const UserFuerzaPublica = require('../models/usuariosFuerzaPublica')
const router = express.Router()
const {transporter} =  require('../emailConfig/email')
const multer = require('multer')

const storage =  multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null,'./uploads/')
    },
    filename: function (req,file, cb) {
        cb(null, new Date().toDateString() + file.originalname)
    }
})

const upload = multer({storage: storage})

router.get("/",[authMiddleware], async function(req,res){
    const result = await User.index(req)
    res.status(200).json(result)
})

router.post('/',asyncM( async (req, res) => {

    // default password before activation
    String.prototype.pick = function(min, max) {
        var n, chars = '';

        if (typeof max === 'undefined') {
            n = min;
        } else {
            n = min + Math.floor(Math.random() * (max - min + 1));
        }

        for (var i = 0; i < n; i++) {
            chars += this.charAt(Math.floor(Math.random() * this.length));
        }

        return chars;
    };


// Credit to @Christoph: http://stackoverflow.com/a/962890/464744
    String.prototype.shuffle = function() {
        var array = this.split('');
        var tmp, current, top = array.length;

        if (top) while (--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
        }

        return array.join('');
    }

    var lowercase = 'abcdefghijklmnopqrstuvwxyz';
    var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';

    var all = lowercase + uppercase + numbers;

    var passwordCreated = '';
    passwordCreated += lowercase.pick(1,2);
    passwordCreated += uppercase.pick(1,2);
    passwordCreated += all.pick(3, 10);
    passwordCreated = passwordCreated.shuffle();
    passwordCreated += Math.floor(Math.random()* 10)
    req.body.contrasena = passwordCreated
    // validate the request body to check if everythings that is need it is there
    const {error} =  User.validateUser(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    // validate if there is already a user with the same email
    let user = await User.validationEmail(req)
    if (!user) return  res.status(400).json({status: 'Usuario ya esta registrado'})
    //  encrypted the password
    const salt = await bcrypt.genSalt(10)
    req.body.contrasena = await bcrypt.hash(passwordCreated,salt)
    // generated the token with the User identification
    // created user in database
    let result = await User.insert(req)
    req.body.idUser = result.idUser
    // filter the results avoiding  the password to be seen
    const token =  await User.generateAuthTokenConfirmation(result.idUser)
    let mailOptions = {
        from: '"Grupo MAT" <confimation@sij.com>', // sender address
        to: req.body.correo , // list of receivers
        subject: 'Confirmacion de usuario ✔', // Subject line
        template: 'email',
        context: {
            token: token,
            constrasena:  passwordCreated
        }

    }
    // send email with a token to confirmated the user
    await transporter.sendMail(mailOptions, async (err) => {
        if (err) {
            return res.status(500).send('Algo salio mal comunicate con el departamento de IT')
        }
        if (req.body.idTipoUsuarios === 4) {
            await UserFuerzaPublica.insert(req)
            return res.status(200).json({status: 'ok', msg: "Usuario registrado correctamente"})
        }
        return res.status(200).json({status: 'ok', msg: "Usuario registrado correctamente"})
    })
//
}))

router.post("/validationEmail", [authMiddleware],  async function(req,res){
    const result = await User.getUserEmail(req)
    res.status(200).json({result: result})
})


router.post("/validationUser", [authMiddleware], async function(req,res){
    const result = await User.validationId(req)
    res.status(200).json({result: result})
})
//
router.get('/:id', [authMiddleware], asyncM( async (req, res) => {
    let user = await User.getUser(req.params.id)
    if (!user) res.status(200).json({error: 'usuario no registrado'})
    if (user.idTipoUsuarios === 4 ) {
        let usuariofuerza = await UserFuerzaPublica.getUserByIdUser(user.idUser)
        let userFinal = {
            Usuario: user,
            InformacionEjercito: usuariofuerza
        }
        res.status(200).json(userFinal)
    }
    let userFinal = {
        Usuario: user,
    }
    res.status(200).json(userFinal)

}))

router.delete('/', [authMiddleware], asyncM( async (req, res) => {
    let user = await User.findById(req.query.id)
    if (!user) return res.status(404).send({error: 'The user with the given ID was not found.'})
    let result = await User.deleteObj(user)
    return res.status(200).send(result)
}))

router.put('/', [authMiddleware], asyncM( async (req, res) => {
    let id = req.query.id || ''
    if (id === '') {
        result = {
            status: false,
            msg : 'id  es requerido como parametro query string'

        }
        return  res.send(result)
    }
    let u = await User.findById(id)
    if (!u)  return  res.send('Usuarios not found')
    const {error} =  User.validateUserUpdate(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    req.body.emailUpdate = u.correo

    let validate = await User.validationEmail(req)
    if (!validate){
        result = {
            status: false,
            msg : 'Usuario con este email ya esta registrado'

        }
        return res.send(result)
    }
    let user = await User.updateMy(req,u)
    if (u.idTipoUsuarios === 4 ) {
        user = await UserFuerzaPublica.updateMy(req, u.idUser)
    }

    //aksdbhkas
    return res.send(user)

}))

router.get('/me',authMiddleware,async (req, res) => {
    const user = await User.findById(req.user._id).select('-password')
    res.send(user)
})

router.post('/confirmation', asyncM( async (req, res) => {

    const {error} =  User.validateUserPasswordReset(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user =  User.validateConfirmationToken(req.body.token)
    console.log(user)
    if (!user) return res.status(400).send({error: 'Invalid Token '})
    user = await User.getUserByIdUser(user._id)
    const salt = await bcrypt.genSalt(10)
    user.contrasena = await bcrypt.hash(req.body.password,salt)
    user.estado = 1
    let result = await user.save()
    result = _.pick(result,['nombres','apellidos','idUser','correo','identification','createdAt','estado'])
    return res.status(200).send( result)
} ))

router.post('/forgotpassword', asyncM( async (req, res) => {

    const {error} =  User.validateUserPasswordReset(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user =  User.validateResetPasswordToken(req.body.token)
    if (!user) return res.status(400).send({error: 'Invalid Token '})
    user = await User.getUserById(user._id)
    const salt = await bcrypt.genSalt(10)
    user.contrasena = await bcrypt.hash(req.body.password,salt)
    const result = await user.save()
    return res.send(_.pick(result,['nombres','apellidos','idUser','correo','identification','createdAt']));

} ))

router.post('/sendResetCode', asyncM( async(req, res) => {

    const {error} = await User.validateUserEmail(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user = await User.getUserEmail(req.body.correo)
    if (!user) return res.status(200).send({msg: 'email send to the user'})
    const token = User.generateAuthTokenPasswordReset(user)
    let mailOptions = {
        from: '"Grupo MAT" <confimation@grupomat.com>', // sender address
        to: req.body.correo , // list of receivers
        subject: 'Reiniciar Contraseña ✔', // Subject line
        template: 'resetPassword',
        context: {
            token: token
        }

    }
    await transporter.sendMail(mailOptions, (err) => {
        if (err) {
            return res.status(500).send('Algo salio mal comunicate con el departamento de IT');
        }
    })
    res.status(200).send({msg: 'email send to the user'})

}))

router.post('/reActiveUser', asyncM( async(req, res) => {

    const {error} = await User.validateUserEmail(req.body)
    if (error) return res.status(400).send({error: error.details[0].message})
    let user = await User.getUserEmail(req.body.correo)
    if (!user) return res.status(400).send({msg: 'Usuario no registrado'})
    const token = User.generateAuthTokenConfirmation(user.idUser)
    let mailOptions = {
        from: '"Grupo MAT" <confimation@grupomat.com>', // sender address
        to: req.body.correo , // list of receivers
        subject: 'Confirmacion de usuario ✔', // Subject line
        template: 'email',
        context: {
            token: token
        }


    }
    await transporter.sendMail(mailOptions, (err) => {
        if (err) {
            return res.status(500).send('Algo salio mal comunicate con el departamento de IT');
        }
    })
    res.status(200).send({msg: 'Token de reactivacion enviado al correo'})

}))

module.exports = router