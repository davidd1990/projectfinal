const express = require('express')
const asyncM = require('../middleware/async')
const authMiddleware = require('../middleware/auth')
const _ = require('lodash')
const FuerzaPublica = require('../models/usuariosFuerzaPublica')
const router = express.Router()
const User = require('../models/usuarios')
const {transporter} =  require('../emailConfig/email')
const multer = require('multer')
const bcrypt = require('bcrypt')
// prueba
router.get("/", async function(req,res){
    const result = await FuerzaPublica.index(req)
    res.status(200).json(result)
})

router.post('/',asyncM( async (req, res) => {

    const {error} =  FuerzaPublica.validateFuerzaPublica(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})
    let usuarioFuerza = await FuerzaPublica.getFuerzaByIdFuerza(req.body)
    if (!usuarioFuerza)  return res.status(400).send({msg: 'Usuario no registrado en la base de datos'})
    String.prototype.pick = function(min, max) {
        var n, chars = '';

        if (typeof max === 'undefined') {
            n = min;
        } else {
            n = min + Math.floor(Math.random() * (max - min + 1));
        }

        for (var i = 0; i < n; i++) {
            chars += this.charAt(Math.floor(Math.random() * this.length));
        }

        return chars;
    };


// Credit to @Christoph: http://stackoverflow.com/a/962890/464744
    String.prototype.shuffle = function() {
        var array = this.split('');
        var tmp, current, top = array.length;

        if (top) while (--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
        }

        return array.join('');
    }

    var lowercase = 'abcdefghijklmnopqrstuvwxyz';
    var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';

    var all = lowercase + uppercase + numbers;

    var passwordCreated = '';
    passwordCreated += lowercase.pick(1,2);
    passwordCreated += uppercase.pick(1,2);
    passwordCreated += all.pick(3, 10);
    passwordCreated = passwordCreated.shuffle();
    passwordCreated += Math.floor(Math.random()* 10)
    req.body.contrasena = passwordCreated

    const salt = await bcrypt.genSalt(10)
    req.body.contrasena = await bcrypt.hash(Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),salt)
    let result = await User.getUserByIdUser(usuarioFuerza.idUser)
    const token =  await User.generateAuthTokenConfirmation(result.idUser)
    let mailOptions = {
        from: '"S.I.J" <confimation@sij.com>', // sender address
        to: result.correo , // list of receivers
        subject: 'Ingreso de usuario ✔', // Subject line
        template: 'email',
        context: {
            token: token,
            constrasena: passwordCreated
        }

    }
    // send email with a token to confirmated the user
    await transporter.sendMail(mailOptions, (err) => {
        if (err) {
            return res.status(500).send('Algo salio mal comunicate con el departamento de IT')
        }
        return res.status(200).send({msg: 'Correo enviado satisfactoriamente'})
    })
    // send email with a token to confirmated the FuerzaPublica
    return res.status(200).send( result)
    // filter the results avoiding  the password to be seen

//
}))

router.post("/validationEmail", [authMiddleware],  async function(req,res){
    const result = await FuerzaPublica.validationEmail(req)
    res.status(200).json({result: result})
})

router.post("/validationFuerzaPublica",[authMiddleware], async function(req,res){
    const result = await FuerzaPublica.validationIdNumber(req)
    res.status(200).json({result: result})
})
//
router.get('/:id', asyncM( async (req, res) => {

    let c = await FuerzaPublica.getFuerzaPublica(req.params.id)
    if (!c) res.status(200).json({error: 'FuerzaPublica no registrada'})
    res.status(200).json(c)
}))

router.get('/identificacion/:id',[authMiddleware], asyncM( async (req, res) => {
    // q paso
    let c = await FuerzaPublica.getFuerzaPublicaById(req.params.id)
    if (!c) res.status(200).json({error: 'FuerzaPublica no registrado'})
    res.status(200).json(c)
}))

router.delete('/', asyncM( async (req, res) => {

    let c = await FuerzaPublica.findById(req.query.id)
    let u = await User.findById(req.query.id)
    if (!c) return res.status(404).send({error: 'The FuerzaPublica with the given ID was not found.'})
    let result = await FuerzaPublica.deleteObj(c)
    await User.deleteObj(u)
    return res.status(200).send(result)
}))

router.put('/', asyncM( async (req, res) => {
    const {error} =  FuerzaPublica.validateFuerzaPublica(req.body)
    let userFuerza = await FuerzaPublica.getFuerzaByIdFuerza(req.identificacionFuerza)
    if (!userFuerza) return res.status(200).send({mgg: 'Se ha enviado un correo con las instrucciones de ingreso'})

    if (error) return res.status(400).json({error: error.details[0].message})
    let c = await FuerzaPublica.updateMy(req)
    //aksdbhkas
    return res.send(c)

}))





module.exports = router