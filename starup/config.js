const config = require('config')
module.exports = function () {

    if (!config.get('jwtPrivateKey')) {
        throw new Error('FATAL ERROR jwtPrivateKey is not define')
    }

    if (!config.get('jwtPrivateKeyEmail')) {
        throw new Error('FATAL ERROR jwtPrivateKeyEmail is not define')
    }
//

}