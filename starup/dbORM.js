const Sequelize = require('sequelize')
const express = require("express")
const app = express()
let db = ''
if (app.get('env') === 'development'){
   db = new Sequelize('biosuppo_dep_defensa', 'biosuppo_project', 'Laravel8012074', {
        host: 'wnks.wnkserver1.com',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
} else {
     db = new Sequelize('biosuppo_dep_defensa', 'biosuppo_project', 'Laravel8012074', {
        host: 'wnks.wnkserver1.com',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
}

module.exports = db

const ciudad = require('../models/ciudad')
const departamento = require('../models/departamento')
const rango = require('../models/rango')
const fuerza = require('../models/tipoFuerzaPublica')
const personalFuerza = require('../models/usuariosFuerzaPublica')
const usuarios = require('../models/usuarios')
const tipoUsuario = require('../models/tipoUser')
const ciudadano = require('../models/ciudadanos')
const lugares = require('../models/lugares')
const modulos = require('../models/modulos')

tipoUsuario.belongsToMany(modulos, { through: 'moduloUser', foreignKey: 'idTipoUsuarios' })
modulos.belongsToMany(tipoUsuario, { as: 'modulo', through: 'moduloUser', foreignKey: 'idmodulo' })
ciudadano.belongsTo(lugares,{foreignKey : 'lugarNacimiento',as: 'lugarNac'})
ciudadano.belongsTo(lugares,{foreignKey : 'lugarExpedicion',as: 'lugarExp'})
usuarios.belongsTo(tipoUsuario,{foreignKey : 'idTipoUsuarios'})
personalFuerza.belongsTo(rango,{foreignKey : 'idRango'})
personalFuerza.belongsTo(fuerza,{foreignKey : 'idFuerza'})
ciudad.belongsTo(departamento,{foreignKey : 'departamento_id'})

