const Sequelize = require('sequelize')
const express = require("express")
const app = express()
let db = ''
if (app.get('env') === 'development'){
    db = new Sequelize('heroku_0c933f0fcbac5cf', 'bd071c313d8d69', '845c0b3f', {
        host: 'us-cdbr-iron-east-03.cleardb.net',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
} else {
    db = new Sequelize('heroku_0c933f0fcbac5cf', 'bd071c313d8d69', '845c0b3f', {
        host: 'us-cdbr-iron-east-03.cleardb.net',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
}

module.exports = db
