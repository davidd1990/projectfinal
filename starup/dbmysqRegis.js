const Sequelize = require('sequelize')
const express = require("express")
const app = express()
let db = ''
if (app.get('env') === 'development'){
    db = new Sequelize('biosuppo_registraduria',  'biosuppo_project', 'Laravel8012074', {
        host: 'wnks.wnkserver1.com',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
} else {
    db = new Sequelize('biosuppo_registraduria', 'biosuppo_project', 'Laravel8012074', {
        host: 'wnks.wnkserver1.com',
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        // SQLite only
        storage: 'path/to/database.sqlite'
    })
}

module.exports = db