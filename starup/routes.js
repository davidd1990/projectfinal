

const user = require("../routes/user")
const auth = require("../routes/auth")
const ciudadano = require("../routes/ciudadanos")
const express = require("express")
const departamento = require("../routes/departamento")
const ciudad = require("../routes/ciudad")
const rango = require("../routes/rango")
const fuerzasPublicas = require("../routes/fuerzaPublica")
const usuarioFuerzaPublica = require("../routes/usuariosFuerzaPublica")
const tipoUsuario = require("../routes/tipoUsuario")

const error = require('../middleware/erros')
const helmet = require("helmet")
module.exports = function (app) {
    //
    app.use(express.json())
    app.use(express.urlencoded({extended: true}))
    app.use(express.static("public"))
    app.use(helmet())
    app.use("/api/user",user)
    app.use("/api/auth",auth)
    app.use("/api/ciudadano",ciudadano)
    app.use("/api/departamento",departamento)
    app.use("/api/ciudad",ciudad)
    app.use("/api/rango",rango)
    app.use("/api/fuerzaPublica",fuerzasPublicas)
    app.use("/api/personalFuerzaPublica",usuarioFuerzaPublica)
    app.use("/api/tipoUser",tipoUsuario)
    app.use(error)
}