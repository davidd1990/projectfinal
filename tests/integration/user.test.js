/* eslint-disable no-undef */
const request = require('supertest')
const {User} = require('../../models/usuarios')
const bcrypt = require('bcrypt')
let server
describe('/api/user', () => {
    beforeEach(()=>{server = require('../../index')})
    afterEach( async () => {
        await User.remove({})
        await server.close()
    })
    //
    describe('validate Body of the request', () => {
        it('Should return 400 if the user name is not send ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({ email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: 'bbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user name is less than 5 character ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'a', email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: 'bbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user name is more than 50 character ',async () =>{
            let name  = Array(52).join('a')
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: name, email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: 'bbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user lastname is not send ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({ name:'aaaaaaaa', email: 'fajke@gmail.com', password: '12345678', isAdmin: true})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user lastname is less than 5 character ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaa', email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: 'b'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user lastname is more than 50 character ',async () =>{
            let lastname  = Array(52).join('a')
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaaa', email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: lastname})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user email is not send ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({ name:'aaaaaaaa', password: '12345678', isAdmin: true, lastname: 'asdasdasdaasd'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user email is less than 5 character ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaa', email: 'f@g', password: '12345678', isAdmin: true, lastname: 'bbbbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user email is more than 255 character ',async () =>{
            let email  = Array(253).join('a')
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaaa', email: email + '@gmail.com', password: '12345678', isAdmin: true, lastname: 'bbbbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user email is not a email ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaaa', email: 'fajkegmailcom', password: '12345678', isAdmin: true, lastname: 'aaaaaaaaa'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the user property isAdmin is not a boolean ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaa', email: 'fake1@gmail.com', password: '12345414', isAdmin: 'asdasdasda', lastname: 'bbbbbbbb'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if is added imputs no valid ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaaaa', email: 'fake1@gmail.com', password: '12345414', isAdmin: true, lastname: 'bbbbbbbb', isConfirmed: true, isActive: true})

            expect(res.status).toBe(400)

        })
    })
    describe('GET/',  () => {
        it('Should return all user',async () => {
            const token = new User({isAdmin: true}).generateAuthToken()
            await User.collection.insertMany([
                {name: 'aaaa', email: 'fake1@fake1.com'},
                {name: 'bbbb', email: 'fake2@fake2.com'}
            ])
            const res = await request(server).get('/api/user')
                .set('x-auth-token', token)
            expect(res.status).toBe(200)
            expect(res.body.length).toBe(2)
            expect(res.body.some(u => u.email === 'fake1@fake1.com')).toBeTruthy()
            expect(res.body.some(u => u.email === 'fake2@fake2.com')).toBeTruthy()
        })
        //a
        it('Should return the user required',async () => {

            let user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            user = await user.save()
            const token = await user.generateAuthToken()
            const res = await request(server).get('/api/user/'+ user._id)
                .set('x-auth-token', token)
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('name', user.name)
        })
        //
        it('Should return 404  if invalid id is passed',async () => {
            const token = new User().generateAuthToken()
            const res = await request(server).get('/api/user/1')
                .set('x-auth-token', token)
            expect(res.status).toBe(404)
            expect(res.body).toHaveProperty('error', 'The user with the given ID is not valid')
        })
        //
    })
    //a
    describe('POST/',  () => {
        it('Should return 200 if user is added ',async () =>{
            const token = new User().generateAuthToken()
            const res = await request(server).post('/api/user')
                .set('x-auth-token', token)
                .send({name: 'aaaaaa', email: 'fajke@gmail.com', password: '12345678', isAdmin: true, lastname: 'bbbbbb'/* role: 'role',actions: [{api_name: 'user', actions :['create']},{api_name: 'auth', actions :['create']}]*/})
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('id')
            expect(res.body).toHaveProperty('name')
        })
    })
    describe('DELETE/', () => {
        //a
        it('Should return 200 if the user was delete ',async () => {
            let user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            user = await user.save()
            const token = await user.generateAuthToken()
            const res = await request(server).delete('/api/user/'+ user._id)
                .set('x-auth-token', token)
            expect(res.status).toBe(200)
        })
        it('Should return 404 if the user does not  exist ', async () => {
            let user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            user = await user.save()
            const token = await user.generateAuthToken()
            const res = await request(server).delete('/api/user/5b3ee4e1b71fa036e4983b74')
                .set('x-auth-token', token)
            expect(res.status).toBe(404)
        })
    })
    describe('PUT/', () => {
        //a
        it('Should return 200 if the user was update ',async () => {

            let user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            user = await user.save()
            const token = await user.generateAuthToken()
            const res = await request(server).put('/api/user/'+ user._id)
                .set('x-auth-token', token)
                .send({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            expect(res.status).toBe(200)
        })
        it('Should return 404 if the user does not  exist ', async () => {
            let user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            user = await user.save()
            const token = await user.generateAuthToken()
            const res = await request(server).put('/api/user/5b3ee4e1b71fa036e4983b74')
                .set('x-auth-token', token)
                .send({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            expect(res.status).toBe(404)
        })
    })


})

describe('api/auth', () => {
    beforeEach(()=>{server = require('../../index')})
    afterEach( async () => {
        await User.remove({})
        await server.close()
    })
    describe('POST/', () => {
        it('should return 200 if user is authorized', async () => {

            const salt = await bcrypt.genSalt(10)
            let password = await bcrypt.hash('12345678',salt)
            const user = new User({email:'fake3@email', name: 'aaaaaaa', lastname: 'aaaaaa', password: password,isAdmin: true, isActive: true, isConfirmed: true})
            await user.save()
            const res = await request(server)
                .post('/api/auth')
                .send({email: 'fake3@email',password:'12345678' })
            expect(res.status).toBe(200)

        } )
        it('should return 400 if user is not authorized for incorrect password', async () => {

            const salt = await bcrypt.genSalt(10)
            let password = await bcrypt.hash('12345678',salt)
            const user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password: password,isAdmin: true, isActive: true, isConfirmed: true})
            await user.save()
            const res = await request(server)
                .post('/api/auth')
                .send({email: 'fake@email',password:'false' })
            expect(res.status).toBe(400)

        } )
        it('should return 400 if user is not authorized for incorrect email', async () => {

            const salt = await bcrypt.genSalt(10)
            let password = await bcrypt.hash('12345678',salt)
            const user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password: password,isAdmin: true, isActive: true, isConfirmed: true})
            await user.save()
            const res = await request(server)
                .post('/api/auth')
                .send({email: 'fake1@email',password:'12345678' })
            expect(res.status).toBe(400)

        })
        it('should return 400 if user is not authorized because is not confirmed', async () => {

            const salt = await bcrypt.genSalt(10)
            let password = await bcrypt.hash('12345678',salt)
            const user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password: password,isAdmin: true, isConfirmed: false})
            await user.save()
            const res = await request(server)
                .post('/api/auth')
                .send({email: 'fake@email',password:'12345678' })
            expect(res.status).toBe(400)
            expect(res.body).toHaveProperty('error', 'User is not confirmed comunicate with IT or see you email for comfimation token')

        })
        it('should return 400 if user is not authorized because is not actived', async () => {

            const salt = await bcrypt.genSalt(10)
            let password = await bcrypt.hash('12345678',salt)
            const user = new User({email:'fake@email', name: 'aaaaaaa', lastname: 'aaaaaa', password: password,isAdmin: true})
            await user.save()
            const res = await request(server)
                .post('/api/auth')
                .send({email: 'fake@email',password:'12345678' })
            expect(res.status).toBe(400)

        },70000)
    })

})

describe('api/user/confirmation', () => {
    beforeEach(()=>{server = require('../../index')})
    afterEach( async () => {
        await User.remove({})
        await server.close()
    })
    it('Should return 400 Invalid token',async () => {
        const res = await request(server).post('/api/user/confirmation')
            .send({token: '1234'})
        expect(res.status).toBe(400)
    })
    //a
    it('Should return 200 if is a valid token',async () => {
        const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
        await user.save()
        const token = user.generateAuthTokenConfirmation()

        const res = await request(server).post('/api/user/confirmation')
            .send({token: token, password: '12345657', password_confirmation:'12345657'})
        expect(res.status).toBe(200)
    })

    it('Should return 400 if is a invalid body',async () => {
        const res = await request(server).post('/api/user/confirmation')
            .send({token: '1234515',password: '12345657', password_confirmation:'12345657'})
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('error', 'Invalid Token ')
    })
    it('Should return 400 if the passwords dont match',async () => {
        const res = await request(server).post('/api/user/confirmation')
            .send({token: '1234515',password: '12345657', password_confirmation:'123457'})
        expect(res.status).toBe(400)
        expect(res.body).toHaveProperty('error', '"password_confirmation" must match password')
    })
})

describe('api/user/forgotpassword', () => {
    beforeEach(()=>{server = require('../../index')})
    afterEach( async () => {
        await User.remove({})
        await server.close()
    })
    describe('POST/', () => {
        it('Should return 200 if the user change its password', async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const token = user.generateAuthTokenPasswordReset()
            const res = await request(server).post('/api/user/forgotpassword')
                .send({token: token, password: '12345657', password_confirmation:'12345657'})
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('name', 'aaaaaaa')
            expect(res.body).toHaveProperty('email', 'fake4@email')
            expect(res.body).toHaveProperty('lastname', 'aaaaaa')
        })
        it('Should return 400 if the token is not valid', async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            user.generateAuthTokenPasswordReset()
            const res = await request(server).post('/api/user/forgotpassword')
                .send({token: '123456', password: '12345657',password_confirmation:'12345657'})
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the  passwords dont match ', async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const token =  user.generateAuthTokenPasswordReset()
            const res = await request(server).post('/api/user/forgotpassword')
                .send({token: token, password: '12345657',password_confirmation:'123456'})

            expect(res.body).toHaveProperty('error','"password_confirmation" must match password')
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the password is less than 5 character ', async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const token =  user.generateAuthTokenPasswordReset()
            const res = await request(server).post('/api/user/forgotpassword')
                .send({token: token, password: '1234',password_confirmation:'1234'})

            expect(res.body).toHaveProperty('error','"password" length must be at least 5 characters long')
            expect(res.status).toBe(400)
        })
        it('Should return 400 if the password is less than 255 character ', async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            const password = Array(257).join('a')
            await user.save()
            const token =  user.generateAuthTokenPasswordReset()
            const res = await request(server).post('/api/user/forgotpassword')
                .send({token: token, password: password,password_confirmation:password})
            expect(res.body).toHaveProperty('error','"password" length must be less than or equal to 255 characters long')
            expect(res.status).toBe(400)
        })
    })
})

describe('api/user/sendResetCode', () => {
    beforeEach(()=>{server = require('../../index')})
    afterEach( async () => {
        await User.remove({})
        await server.close()
    })
    describe('POST/', () => {
        it('Should return status 200 if the email was send',async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const res = await request(server).post('/api/user/sendResetCode')
                .send({email: 'fake4@email'})
            expect(res.status).toBe(200)
        })
        it('Should return status 400 if the email is incorrect',async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const res = await request(server).post('/api/user/sendResetCode')
                .send({email: 'fake4asdfasas'})
            expect(res.status).toBe(400)
        })
        it('Should return status 200 if the email was no found',async () => {
            const user = new User({email:'fake4@email', name: 'aaaaaaa', lastname: 'aaaaaa', password:'12345678',isAdmin: true})
            await user.save()
            const res = await request(server).post('/api/user/sendResetCode')
                .send({email: 'fake3@gmail.com'})
            expect(res.status).toBe(200)
        })
    })
})